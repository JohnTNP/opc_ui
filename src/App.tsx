/* eslint-disable @typescript-eslint/no-unused-vars */
import "./App.css";
import { useContext } from "react";
import { Step } from "./components/Step";
import { StepContext } from "./context";

export interface IStep {
  name: string;
  type: string;
  data: string;
  isPassed?: boolean;
}

function App() {
  const steps = useContext(StepContext)

  return (
    <>
    <div className="navbar bg-base-100">
  <a className="btn btn-ghost normal-case text-xl text-white">OPC UI</a>
</div>
        <div className="flex">
          <div className="flex-none">
            <ul className="steps steps-vertical">
              {steps.map((step) => {
                return (
                  <li
                    data-content={`${step.isPassed ? "✓" : ""}`}
                    className={`step ${step.isPassed ? "step-primary" : null}`}
                  >
                    {step.name}
                  </li>
                );
              })}
            </ul>
          </div>
          <div className="divider divider-horizontal"></div>
          <div className="flex-auto">
            {steps.map((step) => {
              return (
                <Step name={step.name} type={step.type} data={step.data} />
              );
            })}
            <button className="btn">Add Step</button>
          </div>
        </div>
    </>
  );
}

export default App;
