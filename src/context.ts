import { createContext } from "react";
import { IStep } from "./App";

export const StepContext = createContext<IStep[]>([
  {
    name: "Read Values",
    type: "read",
    data: "",
  },
  {
    name: "Write Values",
    type: "write",
    data: "",
  },
]);
