import { StepContext } from "../context";
import { useState, useContext, useRef } from "react";
import { IStep } from "../App";

/* eslint-disable @typescript-eslint/no-unused-vars */
interface StepProps {
  name: string;
  type: string;
  data: string;
}

export function Step(props: StepProps) {
  const input1 = useRef<HTMLInputElement>(null);
  const input2 = useRef<HTMLInputElement>(null);
  const input3 = useRef<HTMLInputElement>(null);
  const [steps, setSteps] = useState<IStep[]>(useContext(StepContext));
  const [output, setOutput] = useState<string[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const addStep = async () => {
    setLoading(true);
    setSteps([...steps, { name: "Test", type: "test", data: "test" }]);
    if (!input1.current?.value) return;
    console.log("Fetching");
    const q_string: string =
      input1.current?.value +
      "," +
      input2.current?.value +
      "," +
      input3.current?.value;
    const url = "http://localhost:56664/api/opc?q_str=" + q_string;
    const res = await fetch(url);
    const text = await res.text();
    setOutput(text.split(",").splice(1, 3));
    console.log(text);
    console.log(steps);
    setLoading(false);
  };
  return (
    <StepContext.Provider value={steps}>
      <div className="card w-auto bg-slate-500 shadow-xl my-5 transition-all">
        <div className="card-body bg-slate-800">
          <h2 className="card-title">{props.name}</h2>
          <input ref={input1} placeholder="Tag 1" type="text" className="input input-bordered text-white" />
          <input ref={input2} placeholder="Tag 2" type="text" className="input input-bordered text-white" />
          <input ref={input3} placeholder="Tag 3" type="text" className="input input-bordered text-white" />
          <div className="card-actions justify-end">
            <button className="btn">Add Tag</button>
            <button
              className="btn btn-primary"
              onClick={async () => {
                await addStep();
              }}
            >
              RUN
            </button>
          </div>
          <ul>
            {loading && <span className="loading loading-ring loading-lg text-success"></span>}
            {!loading && output.map((_output) => (
              <li>{_output}</li>
            ))}
          </ul>
        </div>
      </div>
    </StepContext.Provider>
  );
}
